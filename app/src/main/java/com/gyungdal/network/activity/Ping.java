package com.gyungdal.network.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.gyungdal.network.R;
import com.gyungdal.network.sendPing;

/**
 * Created by GyungDal on 2016-05-21.
 */
public class Ping extends AppCompatActivity {
    private EditText eurl;
    private Button bsend;
    private TextView treuslt;
    private sendPing ping;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ping);
        eurl = (EditText)findViewById(R.id.url);
        bsend = (Button) findViewById(R.id.pingSend);
        treuslt = (TextView)findViewById(R.id.pingResult);
        ping = new sendPing();
    }

    public void click(View v){
        switch(v.getId()){
            case R.id.pingSend :
                treuslt.setText(ping.executeCmd(eurl.getText().toString()));
                break;
            default : break;
        }
    }

}