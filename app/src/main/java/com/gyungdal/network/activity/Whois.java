package com.gyungdal.network.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.gyungdal.network.R;
import com.gyungdal.network.WebInfo;

import java.util.concurrent.ExecutionException;

/**
 * Created by GyungDal on 2016-05-21.
 */
public class Whois extends AppCompatActivity {
    private EditText eurl;
    private Button bget;
    private TextView tresult;
    private WebInfo webInfo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webinfo);
        eurl = (EditText)findViewById(R.id.url);
        bget = (Button)findViewById(R.id.get);
        tresult = (TextView)findViewById(R.id.result);
        webInfo = new WebInfo();
    }

    public void click(View v){
        switch(v.getId()){
            case R.id.get :
                try {
                    tresult.setText(webInfo.execute(eurl.getText().toString()).get());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                break;
            default: break;
        }
    }
}
