package com.gyungdal.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import java.util.HashMap;

/**
 * Created by GyungDal on 2016-05-08.
 */
public class Ipconfig {
    private Context context;
    private DhcpInfo con;
    private WifiManager wifiManager;
    private HashMap<String, String> result;
    public Ipconfig(Context context){
        this.context = context;
    }

    public HashMap<String, String> get(){
        process();
        return result;
    }

    private void process(){
        try {
            wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            con = wifiManager.getDhcpInfo();
            if(con.dns1 == 0){
                result = null;
                return;
            }
            result = new HashMap<>();
            result.put("DNS 1", intToIp(Integer.valueOf(con.dns1)));
            result.put("DNS 2", intToIp(Integer.valueOf(con.dns2)));
            result.put("Default Gateway", intToIp(Integer.valueOf(con.gateway)));
            result.put("IP Address", intToIp(Integer.valueOf(con.ipAddress)));
            result.put("Lease Time", String.valueOf(con.leaseDuration));
            result.put("Subnet Mask", intToIp(Integer.valueOf(con.netmask)));
            result.put("Server IP", intToIp(Integer.valueOf(con.serverAddress)));
        }catch(Exception e){
            Log.e("ipconfig", e.getMessage());
        }
    }

    public static String intToIp(long num) {
        return ((num >> 24 ) & 0xFF) + "." +
                ((num >> 16 ) & 0xFF) + "." +
                ((num >>  8 ) & 0xFF) + "." +
                ( num        & 0xFF);
    }
}
