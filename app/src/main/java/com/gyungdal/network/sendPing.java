package com.gyungdal.network;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by GyungDal on 2016-05-08.
 */
public class sendPing {
    public String executeCmd(String cmd){
        try {
            Process p;
            p = Runtime.getRuntime().exec("ping -c 1 -w 1 " + cmd);
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));

            String s;
            String res = "";
            while ((s = stdInput.readLine()) != null) {
                res += s + "\n";
            }
            p.destroy();
            return res;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";

    }
}
