package com.gyungdal.network;

import android.os.AsyncTask;
import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by GyungDal on 2016-05-06.
 */
public class WebInfo extends AsyncTask<String, Integer , String>{
    private static final String SendFormat = "http://who.is/whois/";
    private static final String TAG = "Web who is";

    // 작업을 시작하기 직전에 호출
    @Override
    protected void onPreExecute() {
    }

    @Override
    protected String doInBackground(String... params) {
        try{
            Document doc = Jsoup.connect(SendFormat + params[0]).get();
            Elements elements = doc.select(".raw_data span");
            Element element = elements.get(0);
            String data = element.toString();
            Pattern tag = Pattern.compile("<(\"[^\"]*\"|\'[^\']*\'|[^\'\">])*>");
            Matcher mat = tag.matcher(data);
            data = mat.replaceAll("");
            data = data.replaceAll("&nbsp;", "").replaceAll("&lt;", "").replaceAll("&gt;","");
            return data;
        }catch (Exception e){
            Log.e(TAG, e.getMessage());
        }
        return null;
    }


    // publishProgress() 메서드를 통해 호출. 진행사항을 표시
    @Override
    protected void onProgressUpdate(Integer... progress){

    }

    // 작업 완료 직후에 호출
    @Override
    protected void onPostExecute(String result){

    }

    // 외부에서 강제로 취소할때 호출
    @Override
    protected void onCancelled(){

    }
}